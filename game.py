import pygame
from player import Player
from monster import Monster, Mummy, Alien
from comet import Comet
from sounds import SoundManager

class Game :

    def __init__(self) :

        self.is_playing = False
        self.comet = Comet(self)
        self.all_comets = pygame.sprite.Group()
        self.all_players = pygame.sprite.Group()
        self.player = Player(self)
        self.all_players.add(self.player)
        self.all_monsters = pygame.sprite.Group()
        self.score = 0
        self.sound_manager = SoundManager()
        self.font = pygame.font.Font("assets/score.TTF", 25)
        self.pressed = {}
        self.monster_kill = 0

    def rain_commet(self) :
        list = [0,1,2,3,4,5,6,7,8,9]
        if self.monster_kill == 10 :
            for i in list :
                self.all_comets.add(Comet(self))
            self.monster_kill = 0
        del list


    def start(self) :
        self.is_playing = True
        self.spawn_monster(Mummy)
        self.spawn_monster(Mummy)
        self.spawn_monster(Alien)
        self.rain_commet()

    def add_score(self, points=10) : 
        self.score += points

    def game_over(self) :
        self.all_monsters = pygame.sprite.Group()
        self.all_comets = pygame.sprite.Group()
        self.player.health = self.player.max_health
        self.score = 0
        self.is_playing = False
        self.sound_manager.play('game_over')


    def update(self, screen,) : 

        score_text = self.font.render(f"Score : {self.score}", 1, (0, 0, 0))
        screen.blit(score_text, (20, 20))
        screen.blit(self.player.image, self.player.rect)

        for projectile in self.player.all_projectiles :
            projectile.move()

        for comet in self.all_comets :
            comet.move()

        for monster in self.all_monsters :
            monster.forward()
            monster.update_health_bar(screen)
            monster.update_animation()

        self.player.all_projectiles.draw(screen)

        self.all_monsters.draw(screen)

        self.all_comets.draw(screen)

        if self.pressed.get(pygame.K_RIGHT) and self.player.rect.x + self.player.rect.width < screen.get_width() :
            self.player.move_right()
        if self.pressed.get(pygame.K_LEFT) and self.player.rect.x > 0 :
            self.player.move_left()

        self.player.update_health_bar(screen)
        self.player.update_animation()

        self.update_kill_monster_bar(screen)

    def check_collision(self, sprite, group):
    	return pygame.sprite.spritecollide(sprite, group, False, pygame.sprite.collide_mask)

    def spawn_monster(self, monster_class_name) :
    	self.all_monsters.add(monster_class_name.__call__(self))

    def update_kill_monster_bar(self, surface) :
        monster_kill_bar = (surface.get_width() // 10)*self.monster_kill
        pygame.draw.rect(surface, (60, 60, 60), [0, surface.get_height()-8, surface.get_width(), 9])
        pygame.draw.rect(surface, (60, 60, 200), [0, surface.get_height()-8, monster_kill_bar, 9])
