import pygame
import random

class Comet (pygame.sprite.Sprite) :

	def __init__(self, game) :
		super().__init__()
		self.attack = 10
		self.game = game
		self.velocity = 15
		self.image = pygame.image.load("assets/Comet.png")
		self.image = pygame.transform.scale(self.image, (60, 60))
		self.rect = self.image.get_rect()
		self.rect.x = random.randint(0, 1100)
		self.rect.y = random.randint(-1100, 0)


	def move(self) :
		self.rect.y += self.velocity
		# self.rect.x += random.randint(0, 1100)

		for comet in self.game.check_collision(self, self.game.all_players) :
			self.remove()
			self.game.player.damage(self.attack)
			
		if self.rect.y >= 630 :
			self.remove()

	def remove(self) :
		self.game.all_comets.remove(self)
		self.game.sound_manager.play('meteorite')