import pygame
import random
import animation

class Monster(animation.AnimateSprite):
	"""docstring for Monter"""

	def __init__(self, game, name, size, offset=0):
		super().__init__(name, size)
		self.health = 100
		self.max_health = 100
		self.game = game
		self.attack = 0.5
		self.rect = self.image.get_rect()
		self.rect.x = 1020 + random.randint(0, 300)
		self.rect.y = 540 - offset
		self.loop_amount = 10
		self.start_animation()

	def set_speed(self, speed) :
		self.default_speed = speed
		self.velocity = random.randint(1, 3)
	
	def set_loop_amount(self, amount) :
		self.loop_amount = amount

	def damage(self, amount) :
		self.health -= amount

		if self.health <= 0 :
			self.game.monster_kill += 1
			self.rect.x = 1020 + random.randint(0, 300)
			self.velocity = random.randint(1, self.default_speed)
			self.health = self.max_health 
			self.game.add_score(self.loop_amount)

	def update_animation(self) :
		self.animate(loop=True)


	def update_health_bar(self, surface):
		pygame.draw.rect(surface,  (60, 63, 60), [self.rect.x + 10, self.rect.y - 20, self.max_health, 5])
		pygame.draw.rect(surface, (111, 210, 46), [self.rect.x + 10, self.rect.y - 20, self.health, 5])


	def forward(self) :
		if not self.game.check_collision(self, self.game.all_players) :
			self.rect.x -= self.velocity
		else: 
			self.game.player.damage(self.attack)

class Mummy(Monster) :

	def __init__(self, game) :
		super().__init__(game, "mummy", (130, 130))
		self.set_speed(3)
		self.set_loop_amount(20)

class Alien(Monster) :

	def __init__(self, game) :
		super().__init__(game, "alien", (300, 300), 130)
		self.health = 250
		self.max_health = 250
		self.set_speed(1)
		self.attack = 0.8
		self.set_loop_amount(80)